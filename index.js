#!/usr/bin/env node
const {passwordGen} = require('./src/niceware-generate');
const program = require('commander');

program
  .version('0.0.1')
  .arguments('<bytelen>')
  .usage('<bytelen> -s <s>')
  .option('-s, --seperator <n>', `The character to seperate words. defaults to #."`)
  .on('--help', function(){
    console.log('<bytelen> A number 1-4. 1 is shortest password and 4 is longest. The number 1-4 is multiplied by 2 to get the number of random bytes to use.')
    console.log('Using the niceware library there is no way to guarantee a number of words')
    console.log('Examples:');
    console.log('  $ niceware -l 2 -s ^ ... codein^insisted');
    console.log('  $ niceware -l 3 -s ... codein=insisted=awaits');
  })
  .action(function (len) {
    cmdLength = len;
  })
  .parse(process.argv);

let length = 2;
if(typeof cmdLength !== 'undefined') {
  length = cmdLength;
}

let res = "";
if(length) {
  res = passwordGen(length, program.seperator);
} else {
  res = passwordGen();
}

console.log(res);
