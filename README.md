# nicepasswd

This is a CLI that uses niceware to generate human readable passwords with a separator of your choosing. It provides a quick way of creating passwords from command line.

See Niceware for more details https://www.npmjs.com/package/niceware

## Usage

To install:

```
npm install -g nicepasswd

```

To generate a password with equal sign as seperator

```
$ nicepasswd 2 -s =
shyest=geology

```
A number 1-4 is used for the arg. 1 is shortest (weakest) password and 4 is longest (strongest). The number 1-4 is multiplied by 2 to get the number of random bytes to use for

```

$nicepasswd 4 -s $%
pal$%maturing$%cozy$%testate


```
Per niceware docs:
"Niceware can be used to generate secure, semi-memorable, easy-to-type passphrases. A random 3-5 word phrase in Niceware is equivalent to a strong password for authentication to most online services. For instance, +8svofk0Y1o= and bacca cavort west volley are equally strong (64 bits of randomness)."

## Credits

Thanks to yan for creating niceware which this is based on
