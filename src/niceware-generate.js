const niceware = require('niceware');

exports.passwordGen = (length = 2, seperator = "#") => {
  if(length > 4) length = 4;
  if(length < 2) length = 2;
  const words = niceware.generatePassphrase(length * 2);
  return words.join(seperator);
}

exports.passphraseToBytes = words =>
  niceware.passphraseToBytes(words.split("="));
