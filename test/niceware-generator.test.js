const {passwordGen, passphraseToBytes} = require('./../src/niceware-generate.js');
const chai = require('chai');
const expect = chai.expect;

describe('#passwordGen', function() {
  it('should generate password with two words', function() {
    const getByteLength = str => Buffer.byteLength(str, 'utf8');
    expect(passphraseToBytes(passwordGen(0, "="))).lengthOf(2);
    expect(passphraseToBytes(passwordGen(1, "="))).lengthOf(2);
    expect(passphraseToBytes(passwordGen(2, "="))).lengthOf(2);
    expect(passphraseToBytes(passwordGen(4, "="))).lengthOf(4);
    expect(passphraseToBytes(passwordGen(6, "="))).lengthOf(4);
    expect(passphraseToBytes(passwordGen(8, "="))).lengthOf(4);
    expect(passphraseToBytes(passwordGen(9, "="))).lengthOf(4);
    expect(passphraseToBytes(passwordGen(50, "="))).lengthOf(4);
  })
})
